LOVE ?= love
GO ?= go

watch:
	fd -e "lua" . | entr -rsc '$(LOVE) ./game'

server:
	fd -e "go" . | entr -rsc '$(GO) run ./server'

.PHONY: watch server
