local Vector = require('brinevector')

local player = {}
setmetatable(player, player)

function player:update(delta, moveStick, kickStick)
	-- Movement
	local friction = 0.0001 ^ delta
	local acceleration = moveStick * delta * 320
	self.velocity = self.velocity * friction
	self.velocity = (self.velocity + acceleration):trim(32)
	self.position = self.position + (self.velocity * delta)

	-- Set the kick force
	self.force = (self.velocity * 1.02) + (kickStick * 80)

	-- Keep the player in bounds
	if self.position.x < COURT.left - 2 then
		self.position.x = COURT.left - 2
	end
	if self.position.x > COURT.right + 2 then
		self.position.x = COURT.right + 2
	end
	if self.position.y < COURT.top - 2 then
		self.position.y = COURT.top - 2
	end
	if self.position.y > COURT.bottom + 2 then
		self.position.y = COURT.bottom + 2
	end
end

local function getStart(team)
	if team == 0 then
		return Vector(COURT.left + 3, love.math.random(-16, 16))
	else
		return Vector(COURT.right - 3, love.math.random(-16, 16))
	end
end

function player:reset()
	self.velocity = Vector(0, 0)
	self.position = getStart(self.team)
end

function player:draw(scale, origin)
	-- Subtract 0.5 to set the ball's origin to its center
	local drawX = math.floor(self.position.x - 0.5) * scale
	local drawY = math.floor(self.position.y - 0.5) * scale
	if self.team == 0 then
		love.graphics.setColor(DARK_BLUE)
	else
		love.graphics.setColor(DARK_RED)
	end
	love.graphics.rectangle(
		"fill",
		drawX + origin.x,
		drawY + origin.y,
		2 * scale,
		2 * scale
	)

	if self.team == 0 then
		love.graphics.setColor(BLUE)
	else
		love.graphics.setColor(RED)
	end
	love.graphics.rectangle(
		"fill",
		drawX + origin.x,
		drawY + origin.y - (scale * 2),
		2 * scale,
		2 * scale
	)
end

function player:__call(team)
	return setmetatable({
		position = getStart(team),
		velocity = Vector(0, 0),
		kickForce = Vector(0, 0),
		team = team
	}, { __index = player })
end

return player
