package main

import (
	"fmt"
	"log"
	"net"
)

type message string

func main() {
	messages := make(chan message)
	go server(messages)

	for {
		select {
		case msg := <-messages:
			log.Println(msg)
		}
	}
}

func server(messages chan<- message) {
	log.Println("listening on :6666")
	conn, err := net.ListenPacket("udp", ":6666")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	buf := make([]byte, 1024)
	for {
		_, addr, err := conn.ReadFrom(buf)
		if err != nil {
			log.Fatal(err)
		}
		messages <- message(fmt.Sprintf("%s: %s", addr, buf))
	}
}
