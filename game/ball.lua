local Vector = require('brinevector')

local ball = {
	position = Vector(0, 0),
	velocity = Vector(0, 0),
	spin = 0
}

function ball.kick(force, spin)
	ball.velocity = force
	ball.spin = spin
end

-- Updates the ball's position using delta time
-- Returns a 0 if no goal was scored, 1 if blue scored, 2 if red scored
function ball.update(delta)
	-- Movement
	local friction = 0.05 ^ delta;
	ball.spin = ball.spin * friction
	ball.velocity = ball.velocity * friction
	ball.velocity.angle = ball.velocity.angle + (ball.spin * delta)
	ball.position = ball.position + (ball.velocity * delta)

	-- Make the ball bounce off walls
	if ball.position.x < COURT.left and (ball.position.y < -7 or ball.position.y > 7) then
		ball.position.x = COURT.left
		ball.velocity.x = -ball.velocity.x
	end
	if ball.position.x > COURT.right and (ball.position.y < -7 or ball.position.y > 7) then
		ball.position.x = COURT.right
		ball.velocity.x = -ball.velocity.x
	end
	if ball.position.y < COURT.top then
		ball.position.y = COURT.top
		ball.velocity.y = -ball.velocity.y
	end
	if ball.position.y > COURT.bottom then
		ball.position.y = COURT.bottom
		ball.velocity.y = -ball.velocity.y
	end

	-- GOOAAALLLL
	if ball.position.x < COURT.left - 2 then
		ball.position = Vector(0, 0)
		ball.velocity = Vector(0, 0)
		return 2
	end
	if ball.position.x > COURT.right + 2 then
		ball.position = Vector(0, 0)
		ball.velocity = Vector(0, 0)
		return 1
	end
	return 0
end

function ball.draw(scale, origin)
	-- Subtract 0.5 to set the ball's origin to its center
	local drawX = math.floor(ball.position.x - 0.5) * scale
	local drawY = math.floor(ball.position.y - 0.5) * scale

	love.graphics.setColor(BLACK)
	love.graphics.rectangle(
		"fill",
		drawX + origin.x,
		drawY + origin.y,
		2 * scale,
		2 * scale
	)
end

return ball
