local Socket  = require('socket')
local Player  = require('player')
local Vector  = require('brinevector')

local field   = require('field')
local ball    = require('ball')
local numbers = require('numbers')

ADDRESS       = "localhost"
PORT          = 6666

COURT         = { left = -37, right = 37, top = -23, bottom = 23 }

BLACK         = { 0.274510, 0.258824, 0.368627 }
WHITE         = { 1.000000, 0.933333, 0.800000 }
GREY          = { 0.823529, 0.839216, 0.772549 }
BLUE          = { 0.000000, 0.725490, 0.745098 }
DARK_BLUE     = { 0.082353, 0.470588, 0.549020 }
RED           = { 1.000000, 0.690196, 0.639216 }
DARK_RED      = { 1.000000, 0.411765, 0.450980 }

KEYCONTROLS   = {
	up = { "w", "up" },
	down = { "s", "down" },
	left = { "a", "left" },
	right = { "d", "right" },
}

local game    = {
	-- The width of the window in pixels
	screenWidth = 960,

	-- The height of the window in pixels
	screenHeight = 640,

	-- The number of real pixels in a game pixel
	scale = 10,

	-- The origin is the center of the field, not the center of the game screen
	origin = Vector(960 / 2, (640 / 2) + 30),

	-- A timer in seconds before the next round begins
	startTimer = 0,

	-- A list of locally controlled players
	players = { Player(0), Player(0), Player(1), Player(1) },

	-- Keeps track of the game score
	score = { blue = 0, red = 0 }
}

function love.load()
	love.window.setTitle("score goal soccer game")

	UDP = Socket.udp()
	UDP:settimeout(0)
	UDP:setpeername(ADDRESS, PORT)
	UDP:send("hello")

	love.window.setMode(game.screenWidth, game.screenHeight)
end

local function handleInput(joysticks, i)
	local moveStick = Vector(0, 0)
	local kickStick = Vector(0, 0)
	local spin = 0
	if joysticks[i] then
		local axis1, axis2, axis3, axis4 = joysticks[i]:getAxes()
		moveStick = Vector(axis1, axis2):trim(1)
		kickStick = Vector(axis3, axis4):trim(1)
		if joysticks[i]:isGamepadDown("leftshoulder") then
			spin = spin - 4
		end
		if joysticks[i]:isGamepadDown("rightshoulder") then
			spin = spin + 4
		end
	elseif i == 1 then
		if love.keyboard.isDown(KEYCONTROLS.up[i]) then
			moveStick.y = moveStick.y - 1
		end
		if love.keyboard.isDown(KEYCONTROLS.down[i]) then
			moveStick.y = moveStick.y + 1
		end
		if love.keyboard.isDown(KEYCONTROLS.left[i]) then
			moveStick.x = moveStick.x - 1
		end
		if love.keyboard.isDown(KEYCONTROLS.right[i]) then
			moveStick.x = moveStick.x + 1
		end
		moveStick = moveStick.normalized
	end

	return moveStick, kickStick, spin
end

function love.update(delta)
	if game.startTimer > 0 then
		game.startTimer = game.startTimer - delta
		return
	end
	local joysticks = love.joystick.getJoysticks()
	local totalSpin = 0
	local totalKick
	for i, v in ipairs(game.players) do
		local moveStick, kickStick, spin = handleInput(joysticks, i)
		v:update(delta, moveStick, kickStick)

		-- Kick the ball if the player is close enough
		local ballDistance = v.position - ball.position
		if ballDistance.length < 2 then
			if totalKick == nil then
				totalKick = Vector(0, 0)
			end
			totalSpin = totalSpin + spin
			totalKick = totalKick + v.force
		end
	end

	if totalKick then
		ball.kick(totalKick, totalSpin)
	end

	local scored = ball.update(delta)
	if scored ~= 0 then
		for _, v in ipairs(game.players) do
			v:reset()
		end
		game.startTimer = 1.5
		if scored == 1 then
			game.score.blue = game.score.blue + 1
		else
			game.score.red = game.score.red + 1
		end
	end
end

function love.draw()
	love.graphics.clear(WHITE)
	field.draw(game.scale, game.origin)
	ball.draw(game.scale, game.origin)
	for _, v in ipairs(game.players) do
		v:draw(game.scale, game.origin)
	end
	numbers.draw(game.origin, game.scale, 1, game.score.red)
	numbers.draw(game.origin, game.scale, 0, game.score.blue)
end

function love.resize(width, height)
	game.screenWidth = width
	game.screenHeight = height
	if (width / 3 < height / 2) then
		game.scale = width / 96
	else
		game.scale = height / 64
	end
	game.origin = Vector(width / 2, (height / 2) + (3 * game.scale))
end
