local field = {}

function field.draw(scale, origin)
	love.graphics.setColor(GREY)

	-- top line
	love.graphics.rectangle(
		"fill",
		(-40 * scale) + origin.x,
		(-26 * scale) + origin.y,
		80 * scale,
		2 * scale
	)

	-- bottom line
	love.graphics.rectangle(
		"fill",
		(-40 * scale) + origin.x,
		(24 * scale) + origin.y,
		80 * scale,
		2 * scale
	)

	-- left sidelines
	love.graphics.rectangle(
		"fill",
		(-40 * scale) + origin.x,
		(-26 * scale) + origin.y,
		2 * scale,
		18 * scale
	)
	love.graphics.rectangle(
		"fill",
		(-40 * scale) + origin.x,
		(8 * scale) + origin.y,
		2 * scale,
		18 * scale
	)
	love.graphics.rectangle(
		"fill",
		(-42 * scale) + origin.x,
		(-10 * scale) + origin.y,
		4 * scale,
		2 * scale
	)
	love.graphics.rectangle(
		"fill",
		(-42 * scale) + origin.x,
		(8 * scale) + origin.y,
		4 * scale,
		2 * scale
	)

	-- right sideline
	love.graphics.rectangle(
		"fill",
		(38 * scale) + origin.x,
		(-26 * scale) + origin.y,
		2 * scale,
		18 * scale
	)
	love.graphics.rectangle(
		"fill",
		(38 * scale) + origin.x,
		(8 * scale) + origin.y,
		2 * scale,
		18 * scale
	)
	love.graphics.rectangle(
		"fill",
		(38 * scale) + origin.x,
		(-10 * scale) + origin.y,
		4 * scale,
		2 * scale
	)
	love.graphics.rectangle(
		"fill",
		(38 * scale) + origin.x,
		(8 * scale) + origin.y,
		4 * scale,
		2 * scale
	)

	-- centerline
	love.graphics.rectangle(
		"fill",
		(-1 * scale) + origin.x,
		(-24 * scale) + origin.y,
		2 * scale,
		18 * scale
	)

	love.graphics.rectangle(
		"fill",
		(-6 * scale) + origin.x,
		(-6 * scale) + origin.y,
		12 * scale,
		2 * scale
	)
	love.graphics.rectangle(
		"fill",
		(-6 * scale) + origin.x,
		(-6 * scale) + origin.y,
		2 * scale,
		12 * scale
	)
	love.graphics.rectangle(
		"fill",
		(4 * scale) + origin.x,
		(-6 * scale) + origin.y,
		2 * scale,
		12 * scale
	)
	love.graphics.rectangle(
		"fill",
		(-6 * scale) + origin.x,
		(4 * scale) + origin.y,
		12 * scale,
		2 * scale
	)

	love.graphics.rectangle(
		"fill",
		(-1 * scale) + origin.x,
		(6 * scale) + origin.y,
		2 * scale,
		18 * scale
	)

	-- left goal
	love.graphics.setColor(DARK_BLUE)
	love.graphics.rectangle(
		"fill",
		(-42 * scale) + origin.x,
		(-8 * scale) + origin.y,
		2 * scale,
		16 * scale
	)
	-- right goal
	love.graphics.setColor(DARK_RED)
	love.graphics.rectangle(
		"fill",
		(40 * scale) + origin.x,
		(-8 * scale) + origin.y,
		2 * scale,
		16 * scale
	)
end

return field
