local Vector = require('brinevector')

local numbers = {}

function numbers.draw(origin, scale, team, number)
	-- Get the length of the number (i.e. count the digits)
	local length = math.floor(math.log(number, 10) + 1)
	if number == 0 then length = 1 end -- Manually set 0 since log of 0 is -inf

	-- Set the location and color based on the team
	local location = Vector(0, -33)

	if team == 0 then -- Blue team
		-- An extra -3 to account for the size of the letter for blue
		location.x = -18 - 3 + ((length - 1) * 2)
		love.graphics.setColor(DARK_BLUE)
	end
	if team == 1 then -- Red team
		location.x = 18 + ((length - 1) * 2)
		love.graphics.setColor(DARK_RED)
	end

	-- Draw each digit
	repeat
		numbers.drawDigit[number % 10](origin, scale, location)
		location.x = location.x - 4
		number = math.floor(number / 10)
	until number == 0
end

-- An array of funtions, whose index is the digit it draws
-- Will draw in whatever color is already set
numbers.drawDigit = {}

numbers.drawDigit[0] = function(origin, scale, location)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		1 * scale,
		5 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 4) * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		((location.x + 2) * scale) + origin.x,
		(location.y * scale) + origin.y,
		1 * scale,
		5 * scale
	)
end

numbers.drawDigit[1] = function(origin, scale, location)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		2 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		((location.x + 1) * scale) + origin.x,
		(location.y * scale) + origin.y,
		1 * scale,
		5 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 4) * scale) + origin.y,
		3 * scale,
		1 * scale
	)
end

numbers.drawDigit[2] = function(origin, scale, location)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		((location.x + 2) * scale) + origin.x,
		(location.y * scale) + origin.y,
		1 * scale,
		3 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 2) * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 2) * scale) + origin.y,
		1 * scale,
		3 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 4) * scale) + origin.y,
		3 * scale,
		1 * scale
	)
end

numbers.drawDigit[3] = function(origin, scale, location)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		((location.x + 2) * scale) + origin.x,
		(location.y * scale) + origin.y,
		1 * scale,
		5 * scale
	)
	love.graphics.rectangle(
		"fill",
		((location.x + 1) * scale) + origin.x,
		((location.y + 2) * scale) + origin.y,
		2 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 4) * scale) + origin.y,
		3 * scale,
		1 * scale
	)
end

numbers.drawDigit[4] = function(origin, scale, location)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		1 * scale,
		3 * scale
	)
	love.graphics.rectangle(
		"fill",
		((location.x + 2) * scale) + origin.x,
		(location.y * scale) + origin.y,
		1 * scale,
		5 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 2) * scale) + origin.y,
		3 * scale,
		1 * scale
	)
end


numbers.drawDigit[5] = function(origin, scale, location)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		1 * scale,
		3 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 2) * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		((location.x + 2) * scale) + origin.x,
		((location.y + 2) * scale) + origin.y,
		1 * scale,
		3 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 4) * scale) + origin.y,
		3 * scale,
		1 * scale
	)
end

numbers.drawDigit[6] = function(origin, scale, location)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		1 * scale,
		3 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 2) * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 2) * scale) + origin.y,
		1 * scale,
		3 * scale
	)
	love.graphics.rectangle(
		"fill",
		((location.x + 2) * scale) + origin.x,
		((location.y + 2) * scale) + origin.y,
		1 * scale,
		3 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 4) * scale) + origin.y,
		3 * scale,
		1 * scale
	)
end

numbers.drawDigit[7] = function(origin, scale, location)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		((location.x + 2) * scale) + origin.x,
		(location.y * scale) + origin.y,
		1 * scale,
		5 * scale
	)
end

numbers.drawDigit[8] = function(origin, scale, location)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		1 * scale,
		5 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 2) * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 4) * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		((location.x + 2) * scale) + origin.x,
		(location.y * scale) + origin.y,
		1 * scale,
		5 * scale
	)
end

numbers.drawDigit[9] = function(origin, scale, location)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		(location.y * scale) + origin.y,
		1 * scale,
		3 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 2) * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		(location.x * scale) + origin.x,
		((location.y + 4) * scale) + origin.y,
		3 * scale,
		1 * scale
	)
	love.graphics.rectangle(
		"fill",
		((location.x + 2) * scale) + origin.x,
		(location.y * scale) + origin.y,
		1 * scale,
		5 * scale
	)
end

return numbers
